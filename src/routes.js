import React from 'react';
// import { Route } from 'react-router-dom';
import { Route } from 'react-router';
import ServerList from './containers/ServerList';
import Server from './containers/Server';
import Register from './containers/Register';
import Login from './containers/Login';

export const routePaths = {
  ServerList: '/',
  Server: (serverId = null) => `/server/${serverId || ':serverId'}`,
  Register: '/Register',
  Login: '/Login',
}

export default [
  <Route exact path={routePaths.ServerList} key={routePaths.ServerList} component={ServerList} />,
  <Route path={routePaths.Server()} key={routePaths.Server()} component={Server} />,
  <Route path={routePaths.Register} key={routePaths.Register} component={Register} />,
  <Route path={routePaths.Login} key={routePaths.Login} component={Login} />,
  <Route path='/*' key='/*' component={null} />,
]