import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';


import ServerListReducer from './containers/ServerList/reducer.js';
import { NAMESPACE as serverListReducerKey } from './containers/ServerList/constants';

import AppReducer from './containers/App/reducer.js';
import { NAMESPACE as appReducerKey } from './containers/App/constants';

import AccountReducer from './containers/Account/reducer.js';
import { NAMESPACE as accountReducerKey } from './containers/Account/constants';

import ServerReducer from './containers/Server/reducer.js';
import { NAMESPACE as serverReducerKey } from './containers/Server/constants';

import { routerReducer } from 'react-router-redux';

const accountPersistConfig = {
  key: accountReducerKey,
  storage: storage,
}

const rootReducer = combineReducers(
  {
    [appReducerKey]: AppReducer,
    routing: routerReducer,
    [serverListReducerKey]: ServerListReducer,
    [serverReducerKey]: ServerReducer,
    [accountReducerKey]: persistReducer(accountPersistConfig, AccountReducer),
  }
);

export default rootReducer;