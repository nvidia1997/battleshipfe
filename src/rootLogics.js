import AppLogics from './containers/App/logics';
import ServerListLogics from './containers/ServerList/logics';
import ServerLogics from './containers/Server/logics';
import RegisterLogics from './containers/Register/logics';
import LoginLogics from './containers/Login/logics';


export default[
  ...AppLogics,
  ...ServerListLogics,
  ...ServerLogics,
  ...RegisterLogics,
  ...LoginLogics,
];