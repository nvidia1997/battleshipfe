import { applyMiddleware, createStore } from 'redux';
import { createLogicMiddleware } from 'redux-logic';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';
import rootReducer from './rootReducer';
import rootLogics from './rootLogics';
import { appStartAction } from './containers/App/actions';
import { routerMiddleware } from 'react-router-redux';

const deps = { // optional injected dependencies for logic
  // anything you need to have available in your logic
  // A_SECRET_KEY: 'example',
};

const logicMiddleware = createLogicMiddleware(rootLogics, deps);

const composeEnhancers = composeWithDevTools({
  // options like actionSanitizer, stateSanitizer
});

export const initStore = (history = {}) => {
  const store = createStore(rootReducer, /* initialState, */ composeEnhancers(
    applyMiddleware(
      logicMiddleware,
      routerMiddleware(history),
    ),
    // other store enhancers if any
  ));
  store.dispatch(appStartAction());

  return store;
}