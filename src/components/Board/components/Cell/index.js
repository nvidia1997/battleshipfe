import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'class-names';

class Cell extends PureComponent {
  render() {
    const {
      cell: {
        id: cellId,
        isHit,
        isMiss,
        isShipPart,
        x,
        y,
      },
      shootAt,
    } = this.props;

    const canShoot = Boolean(shootAt) && !(isMiss || isHit);

    const cellClassName = classNames({
      cell: true,
      'opponent-cell': canShoot,
      'is-hit': isHit,
      'is-miss': isMiss,
      'is-ship-part': !canShoot && isShipPart,
    });

    return (
      <div
        className={cellClassName}
        style={
          {
            gridArea: `${x + 1} / ${y + 1}`,// +1 because x & y are zero based
          }
        }
        onClick={() => canShoot && shootAt(cellId)}
      />
    );
  }
}

Cell.propTypes = {
  cell: PropTypes.object.isRequired,
  shootAt: PropTypes.func,
}

export default Cell;