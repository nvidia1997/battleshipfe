import React, { PureComponent } from 'react';
import Cell from './components/Cell';
import PropTypes from 'prop-types';

import './stylesheets/main.scss';

class Board extends PureComponent {
  render() {
    const {
      board: {
        cells,
      },
      shootAt,
    } = this.props;

    return (
      <div className='board-container'>
        {cells.map((cell, i) => <Cell
          cell={cell}
          key={cell.id}
          shootAt={shootAt}
        />)}
      </div>
    );
  }
}

Board.propTypes = {
  board: PropTypes.object.isRequired,
  shootAt: PropTypes.func,
}

export default Board;