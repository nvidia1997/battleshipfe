import React from 'react';
import Proptypes from 'prop-types';
import classNames from 'class-names';
import { Link  } from 'react-router-dom';

import './stylesheets/main.scss';

function Button(props) {
  const {
    onClick,
    text,
    isEnabled,
    isLink,
    url,
  } = props;

  const buttonClassName = classNames({
    button: true,
    disabled: !isEnabled,
  });

  return (
    <div className={buttonClassName} onClick={() => isEnabled && onClick ? onClick() : null}>
      {
        isEnabled && isLink && url
          ?
          <Link to={url}>{text}</Link>
          :
          text
      }
    </div>
  )
}

Button.propTypes = {
  onClick: Proptypes.func,
  text: Proptypes.string.isRequired,
  url: Proptypes.string,
  isEnabled: Proptypes.bool,
  isLink: Proptypes.bool,
}

Button.defaultProps = {
  isEnabled: true,
  isLink: false,
}

export default Button;
