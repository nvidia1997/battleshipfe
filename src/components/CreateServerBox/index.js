import React, { useState } from 'react';
import Proptypes from 'prop-types';
import Button from '../Button';

function CreateServerBox(props) {
  const { onCreateServerClick } = props;
  const [serverName, setServerName] = useState('defaultServerName');

  return (
    <div>
      <input type='text' value={serverName} onChange={(e) => setServerName(e.target.value)} />
      <Button onClick={() => onCreateServerClick(serverName)} text='Create server' />
    </div>)
}

CreateServerBox.propTypes = {
  onCreateServerClick: Proptypes.func.isRequired
}

export default CreateServerBox;
