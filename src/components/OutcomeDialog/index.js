import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'class-names';
import Button from '../Button';

import './stylesheets/main.scss';

class OutcomeDialog extends PureComponent {
  render() {
    const {
      showWinnerDialog,
      leaveServer,
    } = this.props;

    const text = showWinnerDialog ? 'You win!' : 'You lose!';

    const dialogClassNames = classNames({
      dialog: true,
      'winner-dialog': showWinnerDialog,
      'loser-dialog': !showWinnerDialog,
    });

    return (
      <div className={dialogClassNames}>
        <p>{text}</p>
        <Button text='Leave Server' isEnabled={true} onClick={() => leaveServer()} />
      </div>
    );
  }
}

OutcomeDialog.propTypes = {
  showWinnerDialog: PropTypes.bool.isRequired,
  leaveServer: PropTypes.func.isRequired,
}

export default OutcomeDialog;