import React from 'react';

import './stylesheets/main.scss';

function Spinner() {
  return (
    <div className='spinner' />
  )
}

export default Spinner;
