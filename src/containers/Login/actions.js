import {
  LOGIN_USER,
  USER_LOGGED,
  LOGOUT,
} from "./constants"


/**
* @param {object} credentials 
* @param {string} credentials.username 
* @param {string} credentials.password 
*/
export const loginUserAction = (credentials) => {
  return {
    type: LOGIN_USER,
    payload: credentials,
  }
}

/**
* @param {object} user 
*/
export const userLoggedAction = (user) => {
  return {
    type: USER_LOGGED,
    payload: user,
  }
}

export const logoutAction = () => {
  return {
    type: LOGOUT,
  }
}

export const userLoggedOutAction = () => {
  return {
    type: USER_LOGGED,
  }
}