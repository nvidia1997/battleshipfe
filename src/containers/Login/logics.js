import { createLogic } from 'redux-logic';
import {
  API_ENDPOINTS,
} from '../../utils/API';
import HttpClient from '../../utils/HttpClient';
import { LOGIN_USER, LOGOUT } from './constants';
import { userLoggedAction, userLoggedOutAction } from './actions';
import { routePaths } from '../../routes';
import { push } from 'react-router-redux';

const loginUserLogic = createLogic({
  type: LOGIN_USER,
  async  process({ getState, action }, dispatch, done) {
    try {
      var data = await HttpClient.post(API_ENDPOINTS.ACCOUNTS.POST_METHODS.Login, action.payload);
      dispatch(userLoggedAction(data.user));
      dispatch(push(routePaths.ServerList));
    } catch (error) {
      console.log(error);
    }
    finally {
      done();
    }
  }
});

const logoutLogic = createLogic({
  type: LOGOUT,
  async  process({ getState, action }, dispatch, done) {
    try {
      await HttpClient.post(API_ENDPOINTS.ACCOUNTS.POST_METHODS.Logout, {});
      dispatch(userLoggedOutAction());
      dispatch(push(routePaths.Login));
    } catch (error) {
      console.log(error);
    }
    finally {
      done();
    }
  }
});

export default [
  loginUserLogic,
  logoutLogic,
];