export const NAMESPACE = 'Login';

export const LOGIN_USER = `${NAMESPACE}/LOGIN_USER`;
export const LOGOUT = `${NAMESPACE}/LOGOUT`;
export const USER_LOGGED = `${NAMESPACE}/USER_LOGGED`;
export const USER_LOGGEDOUT= `${NAMESPACE}/USER_LOGGEDOUT`;