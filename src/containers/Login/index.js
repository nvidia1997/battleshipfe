import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from '../../components/Button';
import { loginUserAction } from './actions';


class Login extends Component {
  state = {
    username: '',
    password: '',
  }

  setUsername = (username) => this.setState({ username });
  setPassword = (password) => this.setState({ password });

  render() {
    const {
      username,
      password,
    } = this.state;

    const { login } = this.props;

    const canLogin = !!username && !!password;

    const credentials = {
      username,
      password,
    };

    return (
      <div className='form'>
        <div className='group'>
          <label htmlFor='username'></label>
          <input type='text' name='username' placeholder='username' value={username} onChange={(e) => this.setUsername(e.target.value)} />
        </div>

        <div className='group'>
          <label htmlFor='password'></label>
          <input type='password' name='password' placeholder='password' value={password} onChange={(e) => this.setPassword(e.target.value)} />
        </div>

        <Button text='Login' isEnabled={canLogin} onClick={() => login(credentials)} />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    login: (credentials) => dispatch(loginUserAction(credentials)),
  };
}

Login.propTypes = {
  login: PropTypes.func.isRequired,
}

// Wrap the component to inject dispatch and state into it
export default connect(undefined, mapDispatchToProps)(Login);