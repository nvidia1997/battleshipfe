import {
  AUTHENTICATE_USER,
  USER_AUTHENTICATED,
} from "./constants"

/**
 * @param {String} username 
 */
export const authenticateUserAction = (username) => {
  return {
    type: AUTHENTICATE_USER,
    payload: username,
  }
}

/**
 * @param {Object} user 
 */
export const userAuthenticatedAction = (user) => {
  return {
    type: USER_AUTHENTICATED,
    payload: user,
  }
}