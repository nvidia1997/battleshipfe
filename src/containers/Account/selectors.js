import { createSelector } from 'reselect'
import { NAMESPACE as accountStateKey } from './constants';

const State = state => state[accountStateKey];

export const selectIsAuthenticated = createSelector(
  State,
  /**
  * @returns {boolean}
   */
  (state) => Boolean(state.user) && typeof state.user === 'object'
);

export const selectUserId = createSelector(
  State,
  /**
  * @returns {boolean}
   */
  (state) => state.user && state.user.id
);

export const selectUsername = createSelector(
  State,
  /**
  * @returns {String}
   */
  (state) => state.user && state.user.username
);