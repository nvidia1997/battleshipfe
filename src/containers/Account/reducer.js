import {
  USER_LOGGED,
  USER_LOGGEDOUT
} from "../Login/constants";

const initialState = {
  user: null,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case USER_LOGGED:
      return { ...state, user: payload };
    case USER_LOGGEDOUT:
      return { ...state, user: initialState.user };
    default:
      return state;
  }
}