import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from "react-router";
import { loadServersAction, createServerAction } from './actions';
import { selectServers } from './selectors';
import CreateServerBox from '../../components/CreateServerBox';
import { selectIsBattleshipHubConnected } from '../App/selectors';

import './stylesheets/main.scss';
import Button from '../../components/Button';
import Spinner from '../../components/Spinner';
import { MAX_PLAYERS_COUNT } from './constants';
import { routePaths } from '../../routes';
import { joinServerAction } from '../Server/actions';
import { selectIsAuthenticated } from '../Account/selectors';

class ServerList extends Component {
  componentDidMount() {
    this.props.loadServers();
  }

  /**
   * @param {Array} servers 
   */
  renderServers(servers) {
    const {
      joinServer,
      isAuthenticated,
    } = this.props;

    return servers.map(server => {
      const { playersCount } = server;

      const isButtonEnabled = isAuthenticated && playersCount < MAX_PLAYERS_COUNT

      return (
        <div className='server' key={server.name}>
          <div className='name'>{server.name}</div>
          <div className='players-count'>{playersCount}/{MAX_PLAYERS_COUNT}</div>
          <Button
            isEnabled={isButtonEnabled}
            isLink={false} text={'Join'}
            onClick={() => joinServer(server.id)}
            url={routePaths.Server(server.id)}
          />
        </div>
      )
    }
    )
  }

  render() {
    const {
      servers,
      createServer,
      isBattleshipHubConnected,
    } = this.props;

    if (!isBattleshipHubConnected) {
      return (<Spinner />);
    }

    return (
      <div className='sections-container'>
        <div className='section section-1'>
          <div className='servers'>
            <div className='header'>
              <div className='column'>server name</div>
              <div className='column'>players</div>
            </div>
            {servers && this.renderServers(servers)}
          </div>
        </div>
        <div className='section section-2'>
          {<CreateServerBox onCreateServerClick={createServer} />}
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadServers: () => dispatch(loadServersAction()),
    createServer: (serverName) => dispatch(createServerAction(serverName)),
    joinServer: (serverId) => dispatch(joinServerAction(serverId)),
  };
}

const mapStateToProps = (state) => {
  return {
    servers: selectServers(state),
    isBattleshipHubConnected: selectIsBattleshipHubConnected(state),
    isAuthenticated: selectIsAuthenticated(state),
  }
};

ServerList.propTypes = {
  loadServers: PropTypes.func.isRequired,
  isBattleshipHubConnected: PropTypes.bool.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  servers: PropTypes.array.isRequired,
}

// Wrap the component to inject dispatch and state into it
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ServerList));
