import { createSelector } from 'reselect'
import { NAMESPACE as serverListStateKey } from './constants';

const State = state => state[serverListStateKey];

export const selectServers = createSelector(
  State,
  (state) => state.servers
);