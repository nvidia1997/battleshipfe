import {
  LOAD_SERVERS,
  CREATE_SERVER,
  SERVER_CREATED,
  SERVERS_LOADED,
  SHORT_SERVER_UPDATED,
  SERVER_DELETED,
} from "./constants"

export const loadServersAction = () => {
  return {
    type: LOAD_SERVERS
  }
}

export const serversLoadedAction = (servers) => {
  return {
    type: SERVERS_LOADED,
    payload: servers
  }
}

/**
 * @param {String} serverName 
 */
export const createServerAction = (serverName) => {
  return {
    type: CREATE_SERVER,
    payload: serverName
  }
}

/**
 * @param {Object} server 
 */
export const serverCreatedAction = (server) => {
  return {
    type: SERVER_CREATED,
    payload: server
  }
}

/**
 * @param {Object} server 
 */
export const shortServerUpdatedAction = (server) => {
  return {
    type: SHORT_SERVER_UPDATED,
    payload: server,
  }
}

/**
 * @param {number} serverId 
 */
export const serverDeletedAction = (serverId) => {
  return {
    type: SERVER_DELETED,
    payload: serverId,
  }
}