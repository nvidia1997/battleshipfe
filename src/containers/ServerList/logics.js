import { createLogic } from 'redux-logic';

import {
  LOAD_SERVERS,
  CREATE_SERVER,
} from './constants';
import { APP_STARTED } from '../App/constants';
import { selectBattleshipHubConnection } from '../App/selectors';
import {
  WEBSOCKET_EVENTS,
  API_ENDPOINTS,
} from '../../utils/API'
import HttpClient from '../../utils/HttpClient';
import {
  serversLoadedAction,
  serverCreatedAction,
  shortServerUpdatedAction,
  serverDeletedAction,
} from './actions';


const appStartedLogic = createLogic({
  type: APP_STARTED,
  process({ getState, action }, dispatch, done) {
    const currentState = getState();
    const battleshipHubConnection = selectBattleshipHubConnection(currentState);

    battleshipHubConnection.on(WEBSOCKET_EVENTS.SERVER_CREATED, data => {
      dispatch(serverCreatedAction(data.server));
    });

    battleshipHubConnection.on(WEBSOCKET_EVENTS.SHORT_SERVER_UPDATED, (data) => {
      dispatch(shortServerUpdatedAction(data.server));
    });

    battleshipHubConnection.on(WEBSOCKET_EVENTS.SERVER_DELETED, (serverId) => {
      dispatch(serverDeletedAction(serverId));
    });

    // this logic should be never ending because of the event hooks
  }
});

const createServerLogic = createLogic({
  type: CREATE_SERVER,
  async process({ getState, action }, dispatch, done) {
    try {
      const serverName = action.payload;
      await HttpClient.post(API_ENDPOINTS.SERVERS.POST_METHODS.Create, { serverName });
    } catch (error) {
      console.log(error);
    }
    finally {
      done();
    }
  }
});

const loadServersLogic = createLogic({
  type: LOAD_SERVERS,
  async process({ getState, action }, dispatch, done) {
    try {
      const data = await HttpClient.get(API_ENDPOINTS.SERVERS.GET_METHODS.Get);

      dispatch(serversLoadedAction(data.servers));
    } catch (error) {
      console.log(error);
    }
    finally {
      done();
    }
  }
});

export default [
  appStartedLogic,
  loadServersLogic,
  createServerLogic,
];