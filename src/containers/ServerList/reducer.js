import {
  SERVERS_LOADED,
  SERVER_CREATED,
  SHORT_SERVER_UPDATED,
  SERVER_DELETED,
} from "./constants";

const initialState = {
  servers: [],
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SERVERS_LOADED:
      return { ...state, servers: payload };
    case SERVER_CREATED:
      return { ...state, servers: [...state.servers, payload] };
    case SERVER_DELETED:
      return { ...state, servers: state.servers.filter(x => x.id !== payload) };
    case SHORT_SERVER_UPDATED:
      return {
        ...state,
        servers: state.servers.map((server) => {
          if (server.id === payload.id) {
            return payload;
          }
          return server;
        })
      };
    default:
      return state;
  }
}

