/* eslint-disable no-unused-vars */
import { createSelector } from 'reselect'
import {
  HubConnection,
} from '@aspnet/signalr';//used to enable intellisense
import { NAMESPACE as serverListStateKey } from './constants';

const State = state => state[serverListStateKey];

export const selectBattleshipHubConnection = createSelector(
  State,
  /**
  * @returns {HubConnection} hubConnection
   */
  (state) => state.battleshipHubConnection
);

export const selectIsBattleshipHubConnected = createSelector(
  State,
  /**
  * @returns {boolean}
   */
  (state) => state.isBattleshipHubConnected
);