/* eslint-disable no-unused-vars */
import { createLogic } from 'redux-logic';
import { APP_START } from './constants';
import {
  appStartedAction,
  battleshipHubConnectionStateChangedAction,
} from './actions';
import { WEBSOCKET_HUBS } from '../../utils/API'
import HubConnectionHelper from '../../utils/HubConnectionHelper';
import { HubConnection } from '@aspnet/signalr';

const appStartLogic = createLogic({
  type: APP_START,
  process({ getState, action }, dispatch, done) {
    const battleshipHubConnection = HubConnectionHelper.buildConnection(WEBSOCKET_HUBS.BATTLESHIP_HUB);
    dispatch(appStartedAction(battleshipHubConnection));

    battleshipHubConnection.onclose((error) => {
      onBattleshipHubConnectionStateChanged(dispatch)
      startConnection(battleshipHubConnection, dispatch);
    })

    startConnection(battleshipHubConnection, dispatch);

    // this logic should be never ending because of the event hooks
  }
});

/**
 * @param {Function} dispatch
 */
const onBattleshipHubConnectionStateChanged = (dispatch) => {
  dispatch(battleshipHubConnectionStateChangedAction());
}

/**
 * @param {HubConnection} connection 
 * @param {Funcion} dispatch 
 */
const startConnection = (connection, dispatch) => {
  HubConnectionHelper.startConnection(connection, () => onBattleshipHubConnectionStateChanged(dispatch));
}

export default [
  appStartLogic,
];