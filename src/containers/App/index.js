import React from 'react';
import { PersistGate } from 'redux-persist/integration/react'
import { Provider } from 'react-redux';
import { initStore } from '../../configureStore';
import routes from '../../routes';
import Header from '../Header';
import { Router } from 'react-router';
import { persistStore } from 'redux-persist'
import { createBrowserHistory } from 'history';
import { syncHistoryWithStore } from 'react-router-redux/lib';

import './stylesheets/main.scss';


function App() {
  const browserHistory = createBrowserHistory();
  const store = initStore(browserHistory);
  const history = syncHistoryWithStore(browserHistory, store);
  const persistor = persistStore(store);

  return (
    <Provider store={store} >
      <PersistGate loading={null} persistor={persistor}>
        <Router history={history}>
          <div className='App'>
            <Header />
            {routes}
          </div>
        </Router>
      </PersistGate>
    </Provider >
  );
}

export default (App);