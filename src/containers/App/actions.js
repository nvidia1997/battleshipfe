/* eslint-disable no-unused-vars */
import {
  APP_START,
  APP_STARTED,
  BATTLESHIP_HUB_CONNECTION_STATE_CHANGED,
} from "./constants"
import { HubConnection } from "@aspnet/signalr"

export const appStartAction = () => {
  return {
    type: APP_START
  }
}

/**
 * @param {HubConnection} connection 
 */
export const appStartedAction = (connection) => {
  return {
    type: APP_STARTED,
    payload: connection
  }
}

export const battleshipHubConnectionStateChangedAction = () => {
  return {
    type: BATTLESHIP_HUB_CONNECTION_STATE_CHANGED
  }
}