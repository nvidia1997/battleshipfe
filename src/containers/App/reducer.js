import {
  APP_STARTED,
  BATTLESHIP_HUB_CONNECTION_STATE_CHANGED,
} from "./constants";
import HubConnectionHelper from "../../utils/HubConnectionHelper";

const initialState = {
  battleshipHubConnection: null,
  isBattleshipHubConnected: false,
  his: false,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case APP_STARTED:
      return { ...state, battleshipHubConnection: payload };
    case BATTLESHIP_HUB_CONNECTION_STATE_CHANGED:
      return { ...state, isBattleshipHubConnected: HubConnectionHelper.isConnected(state.battleshipHubConnection) };
    default:
      return state;
  }
}

