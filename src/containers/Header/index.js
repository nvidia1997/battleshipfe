import React, { Component } from 'react';
import { connect } from 'react-redux';
import Proptypes from 'prop-types';
import { withRouter } from "react-router";
import { selectUsername, selectIsAuthenticated } from '../Account/selectors';

import './stylesheets/main.scss'
import { logoutAction } from '../Login/actions';
import Button from '../../components/Button';
import { routePaths } from '../../routes';

class Header extends Component {
  renderAccountSection(username) {
    return (
      <div className='menu-item'>
        {`Hello, ${username}`}
        <Button text='Logout' onClick={this.props.logout} />
      </div>
    );
  }

  renderAuthButtons() {
    return (
      <div className='menu-item'>
        <Button text='Register' isLink={true} url={routePaths.Register} />
        <Button text='Login' isLink={true} url={routePaths.Login} />
      </div>
    );
  }

  renderNavbar() {
    return (
      <div className='menu-item nav-bar'>
        <Button text='Home' isLink={true} url={routePaths.ServerList} />
      </div>
    );
  }

  render() {
    const {
      isAuthenticated,
      username,
    } = this.props;

    return (
      <header >
        {this.renderNavbar()}
        {isAuthenticated ? this.renderAccountSection(username) : this.renderAuthButtons()}
      </header >
    );
  }
}

const mapStateToProps = (state) => {
  return {
    username: selectUsername(state),
    isAuthenticated: selectIsAuthenticated(state),
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(logoutAction()),
  };
}

Header.propTypes = {
  isAuthenticated: Proptypes.bool.isRequired,
  username: Proptypes.string,
  logout: Proptypes.func.isRequired,
}

// Wrap the component to inject dispatch and state into it
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));