import { REGISTER_USER } from "./constants"


/**
* @param {Object} credentials 
* @param {string} params.username 
* @param {string} params.password 
* @param {string} params.confirmedPassword 
* @param {string} params.email 
*/
export const registerUserAction = (credentials) => {
  return {
    type: REGISTER_USER,
    payload: credentials,
  }
}