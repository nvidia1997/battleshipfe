import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from '../../components/Button';
import { registerUserAction } from './actions';


class Register extends Component {
  state = {
    username: '',
    email: '',
    password: '',
    confirmedPassword: '',
  }

  setUsername = (username) => this.setState({ username });
  setEmail = (email) => this.setState({ email });
  setPassword = (password) => this.setState({ password });
  setConfirmedPassword = (confirmedPassword) => this.setState({ confirmedPassword });

  render() {
    const {
      username,
      email,
      password,
      confirmedPassword,
    } = this.state;

    const { register } = this.props;

    const passwordsMatch = password === confirmedPassword;

    const canRegister = !!username && !!email && !!password && !!confirmedPassword && Boolean(passwordsMatch);

    const credentials = {
      username,
      email,
      password,
      confirmedPassword,
    };

    return (
      <div className='form'>
        <div className='group'>
          <label htmlFor='username'></label>
          <input type='text' name='username' placeholder='username' value={username} onChange={(e) => this.setUsername(e.target.value)} />
        </div>

        <div className='group'>
          <label htmlFor='email'></label>
          <input type='text' name='email' placeholder='email' value={email} onChange={(e) => this.setEmail(e.target.value)} />
        </div>

        <div className='group'>
          <label htmlFor='password'></label>
          <input type='password' name='password' placeholder='password' value={password} onChange={(e) => this.setPassword(e.target.value)} />
        </div>

        <div className='group'>
          <label htmlFor='confirmedPassword'>{!passwordsMatch && 'Passwords do not match'}</label>
          <input type='password' name='confirmedPassword' placeholder='confirmedPassword' value={confirmedPassword} onChange={(e) => this.setConfirmedPassword(e.target.value)} />
        </div>

        <Button text='Register' isEnabled={canRegister} onClick={() => register(credentials)} />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    register: (params) => dispatch(registerUserAction(params)),
  };
}

Register.propTypes = {
  register: PropTypes.func.isRequired,
}

// Wrap the component to inject dispatch and state into it
export default connect(undefined, mapDispatchToProps)(Register);