import { createLogic } from 'redux-logic';
import {
  API_ENDPOINTS,
} from '../../utils/API';
import HttpClient from '../../utils/HttpClient';
import { REGISTER_USER } from './constants';
import { routePaths } from '../../routes';
import { push } from 'react-router-redux';

const registerLogic = createLogic({
  type: REGISTER_USER,
  async  process({ getState, action }, dispatch, done) {
    try {
      await HttpClient.post(API_ENDPOINTS.ACCOUNTS.POST_METHODS.Register, action.payload);
      dispatch(push(routePaths.Login));
    } catch (error) {
      console.log(error);
    }
    finally {
      done();
    }
  }
});

export default [
  registerLogic,
];