import {
  SERVER_JOINED,
  JOIN_SERVER,
  LEAVE_SERVER,
  SHOOT_AT_CEll,
  SERVER_UPDATED,
  LOAD_SERVER,
} from "./constants"

export const leaveServerAction = () => {
  return {
    type: LEAVE_SERVER,
  }
}

/**
* @param {number} serverId 
*/
export const joinServerAction = (serverId) => {
  return {
    type: JOIN_SERVER,
    payload: serverId,
  }
}

/**
* @param {number} serverId 
*/
export const loadServerAction = (serverId) => {
  return {
    type: LOAD_SERVER,
    payload: serverId,
  }
}

/**
 * @param {Object} server 
 */
export const serverJoinedAction = (server) => {
  return {
    type: SERVER_JOINED,
    payload: server,
  }
}

/**
 * @param {number} cellId 
 */
export const shootAtCellAction = (cellId) => {
  return {
    type: SHOOT_AT_CEll,
    payload: cellId,
  }
}

/**
 * @param {Object} server 
 */
export const serverUpdatedAction = (server) => {
  return {
    type: SERVER_UPDATED,
    payload: server,
  }
}