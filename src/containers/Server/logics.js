import { createLogic } from 'redux-logic';
import { selectBattleshipHubConnection } from '../App/selectors';
import {
  WEBSOCKET_EVENTS,
  API_ENDPOINTS,
} from '../../utils/API';
import { APP_STARTED } from '../App/constants';
import {
  JOIN_SERVER,
  LEAVE_SERVER,
  SHOOT_AT_CEll,
  LOAD_SERVER,
} from './constants';
import HttpClient from '../../utils/HttpClient';
import {
  serverJoinedAction,
  serverUpdatedAction,
} from './actions';
import { selectIsAuthenticated } from '../Account/selectors';
import { routePaths } from '../../routes';
import { push } from 'react-router-redux';
import { SERVER_DELETED } from '../ServerList/constants';
import { selectServer } from './selectors';

const appStartedLogic = createLogic({
  type: APP_STARTED,
  process({ getState, action }, dispatch, done) {
    const currentState = getState();
    const battleshipHubConnection = selectBattleshipHubConnection(currentState);

    battleshipHubConnection.on(WEBSOCKET_EVENTS.SERVER_UPDATED, (data) => {
      dispatch(serverUpdatedAction(data.server));
    });

    // this logic should be never ending because of the event hooks
  }
});

const joinServerLogic = createLogic({
  type: JOIN_SERVER,
  async  process({ getState, action }, dispatch, done) {
    const serverId = action.payload;
    try {
      const currentState = getState();
      const isAuthenticated = selectIsAuthenticated(currentState);

      if (!isAuthenticated) { return; }

      const data = await HttpClient.post(API_ENDPOINTS.PLAYERS.POST_METHODS.JoinServer, { serverId });
      dispatch(serverJoinedAction(data));
      dispatch(push(routePaths.Server(serverId)));
    } catch (error) {
      console.log(error);
    }
    finally {
      done();
    }
  }
});

const serverDeletedLogic = createLogic({
  type: SERVER_DELETED,
  async  process({ getState, action }, dispatch, done) {
    const serverId = action.payload;
    const currentServer = selectServer(getState());

    if (currentServer && currentServer.id === serverId) {
      dispatch(push(routePaths.ServerList));
    }
  }
});

const leaveServerLogic = createLogic({
  type: LEAVE_SERVER,
  async  process({ getState, action }, dispatch, done) {
    try {
      await HttpClient.post(API_ENDPOINTS.PLAYERS.POST_METHODS.LeaveServer);
    } catch (error) {
      console.log(error);
    }
    finally {
      dispatch(push(routePaths.ServerList));
      done();
    }
  }
});

const shootAtCellLogic = createLogic({
  type: SHOOT_AT_CEll,
  async  process({ getState, action }, dispatch, done) {
    try {
      const cellId = action.payload;
      await HttpClient.post(API_ENDPOINTS.PLAYERS.POST_METHODS.Shoot, { cellId });
    } catch (error) {
      console.log(error);
    }
    finally {
      done();
    }
  }
});

const loadServerLogic = createLogic({
  type: LOAD_SERVER,
  async  process({ getState, action }, dispatch, done) {
    try {
      const serverId = action.payload;
      const server = await HttpClient.get(API_ENDPOINTS.SERVERS.GET_METHODS.GetServer(serverId));
      dispatch(serverUpdatedAction(server));
    } catch (error) {
      console.log(error);
    }
    finally {
      done();
    }
  }
});

export default [
  appStartedLogic,
  joinServerLogic,
  leaveServerLogic,
  shootAtCellLogic,
  loadServerLogic,
  serverDeletedLogic,
];