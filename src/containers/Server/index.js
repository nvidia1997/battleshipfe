

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from "react-router";
import {
  selectCurrentPlayer,
  selectOpponentPlayer,
  selectPlayersCount,
  selectServer,
} from './selectors';
import {
  joinServerAction,
  leaveServerAction,
  shootAtCellAction,
  loadServerAction,
} from './actions';
import { selectIsAuthenticated } from '../Account/selectors';
import Spinner from '../../components/Spinner';
import Board from '../../components/Board';

import './stylesheets/main.scss';
import { MAX_PLAYERS_COUNT } from '../ServerList/constants';
import OutcomeDialog from '../../components/OutcomeDialog';

class Server extends PureComponent {
  componentDidMount() {
    const { joinServer, server, match } = this.props;
    if (!server) {
      const { serverId } = match.params;
      joinServer(serverId);
    }
  }

  componentDidUpdate() {
    if (!this.props.server) {
      this.loadServer();
    }
  }

  componentWillUnmount() {
    const {
      leaveServer,
    } = this.props;

    leaveServer();
  }

  loadServer() {
    const { isAuthenticated } = this.props;
    if (!isAuthenticated) { return; }

    const { serverId } = this.props.match.params;
    this.props.loadServer(Number(serverId));
  }

  renderBoard(text, board, onShoot) {
    return (
      <div>
        <div className='board-title'>{text}</div>
        <Board
          board={board}
          shootAt={onShoot}
        />
      </div>
    )
  }

  renderLoadingForm(playersCount) {
    return (
      <div className='loading-container'>
        <p className='players-count'>Players:{`${playersCount}/${MAX_PLAYERS_COUNT}`}</p>
        <Spinner />
        <div className='waiting-message'>Waiting for other players to join ...</div>
      </div>
    );
  }

  render() {
    const {
      isAuthenticated,
      currentPlayer,
      opponentPlayer,
      playersCount,
      leaveServer,
      shootAtCell,
    } = this.props;

    const hasWinner = currentPlayer && opponentPlayer
      ? currentPlayer.isLoser || opponentPlayer.isLoser
      : false;

    if (!isAuthenticated || (playersCount < MAX_PLAYERS_COUNT && !hasWinner)) {
      return this.renderLoadingForm(playersCount);
    }

    return (
      <React.Fragment>
        <div className='boards'>
          {currentPlayer && this.renderBoard('My board', currentPlayer.board)}

          {
            opponentPlayer && currentPlayer.isAllowedToShoot
            &&
            this.renderBoard(`${opponentPlayer.username}'s board`, opponentPlayer.board, shootAtCell)
          }
        </div>
        {
          hasWinner
          &&
          <OutcomeDialog
            showWinnerDialog={!currentPlayer.isLoser}
            leaveServer={leaveServer}
          />
        }
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    joinServer: (serverId) => dispatch(joinServerAction(serverId)),
    loadServer: (serverId) => dispatch(loadServerAction(serverId)),
    leaveServer: () => dispatch(leaveServerAction()),
    shootAtCell: (cellId) => dispatch(shootAtCellAction(cellId)),
  };
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: selectIsAuthenticated(state),
    currentPlayer: selectCurrentPlayer(state),
    opponentPlayer: selectOpponentPlayer(state),
    playersCount: selectPlayersCount(state),
    server: selectServer(state),
  }
};

Server.propTypes = {
  playersCount: PropTypes.number.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  leaveServer: PropTypes.func.isRequired,
  shootAtCell: PropTypes.func.isRequired,
  loadServer: PropTypes.func.isRequired,
  joinServer: PropTypes.func.isRequired,
}

// Wrap the component to inject dispatch and state into it
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Server));
