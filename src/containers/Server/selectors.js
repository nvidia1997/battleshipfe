import { createSelector } from 'reselect'
import { NAMESPACE as serverStateKey } from './constants';
import { selectUserId } from '../Account/selectors';

const MainState = state => state;
const State = state => state[serverStateKey];

export const selectServer = createSelector(
  State,
  (state) => state.server
);

export const selectPlayers = createSelector(
  selectServer,
  (serverState) => {
    if (serverState) { return serverState.players }
  }
);

export const selectPlayersCount = createSelector(
  selectServer,
  (serverState) => {
    return serverState && serverState.players ? serverState.players.length : 0;
  }
);

export const selectOpponentPlayer = createSelector(
  selectPlayers,
  MainState,
  (playersState, mainState) => {
    if (playersState) { return playersState.find(x => x.id !== selectUserId(mainState)); }
  }
);

export const selectCurrentPlayer = createSelector(
  selectPlayers,
  MainState,
  (playersState, mainState) => {
    if (playersState) { return playersState.find(x => x.id === selectUserId(mainState)); }
  }
);