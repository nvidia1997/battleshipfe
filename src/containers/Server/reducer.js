import { SERVER_JOINED, SERVER_UPDATED } from "./constants";

const initialState = {
  server: null,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SERVER_JOINED:
    case SERVER_UPDATED:
      return { ...state, server: payload };
    default:
      return state;
  }
}