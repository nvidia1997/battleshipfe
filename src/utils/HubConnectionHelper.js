/* eslint-disable no-unused-vars */
import {
  HubConnection,
  HubConnectionState,
  HubConnectionBuilder,
  HttpTransportType,
} from '@aspnet/signalr';

class HubConnectionHelper {

  /**
   * @param {String} url 
   * @returns {HubConnection} connection 
   */
  static buildConnection(url) {
    return new HubConnectionBuilder()
      .withUrl(
        url,
        {
          skipNegotiation: true,
          transport: HttpTransportType.WebSockets
        })
      .build();
  }

  /**
   * @param {HubConnection} connection 
   * @returns {boolean}
   */
  static isConnected(connection) {
    return connection.state === HubConnectionState.Connected;
  }

  /**
   * @param {HubConnection} connection 
   * @param {Function} [onStart] 
   */
  static startConnection(connection, onStart) {
    connection
      .start()
      .then(() => {
        if (typeof onStart === 'function') {
          onStart();
        }
      })
      .catch(error => {
        console.log(error);
        setTimeout(() => {
          this.startConnection(connection, onStart);//try to reconnect
        }, 40000);
      })
  }
}

export default HubConnectionHelper;