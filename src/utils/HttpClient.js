import axios from 'axios';

export default class HttpClient {
  static headers = {
    'Content-Type': 'application/json'
  }

  /**
   * @param {String} url 
   */
  static get(url) {
    /**
   * @type {import("axios").AxiosRequestConfig}
   */
    const config = {
      withCredentials: true,
      headers: this.headers,
    }
    return axios.get(url, config).then(response => response.data);
  }

  /**
   * @param {String} url 
   * @param {Object} [data] 
   */
  static post(url = '', data = {}) {
    /**
     * @type {import("axios").AxiosRequestConfig}
     */
    const config = {
      withCredentials: true,
      headers: this.headers,
    }

    if (data && Object.keys(data).length > 0) {
      config.data = data;
    }

    return axios.post(url, {}, config).then(response => response.data);
  }
}