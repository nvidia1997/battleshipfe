
import EnvironmentHelper from './EnvironmentHelper';

let API_URL = EnvironmentHelper.isDevelopment ?
  'https://localhost:44392/api'
  :
  `${window.location.origin}/api`;

const WEBSOCKET_ENDPOINT = `${API_URL}/hubs`;

export const API_ENDPOINTS = {
  URL: API_URL,
  SERVERS: {
    GET_METHODS: {
      Get: `${API_URL}/servers/Get`,
      GetServer: (serverId) => `${API_URL}/servers/GetServer?serverId=${serverId}`,
    },
    POST_METHODS: {
      Create: `${API_URL}/servers/Create`,
    }
  },
  PLAYERS: {
    GET_METHODS: {
    },
    POST_METHODS: {
      JoinServer: `${API_URL}/players/JoinServer`,
      LeaveServer: `${API_URL}/players/LeaveServer`,
      Shoot: `${API_URL}/players/Shoot`,
    }
  },
  ACCOUNTS: {
    GET_METHODS: {
    },
    POST_METHODS: {
      Register: `${API_URL}/accounts/Register`,
      Login: `${API_URL}/accounts/Login`,
      Logout: `${API_URL}/accounts/Logout`,
    }
  }
}

export const WEBSOCKET_HUBS = {
  BATTLESHIP_HUB: `${WEBSOCKET_ENDPOINT}/battleshipHub`,
}

export const WEBSOCKET_EVENTS = {
  SERVER_CREATED: 'serverCreated',
  SERVER_DELETED: 'serverDeleted',
  SHORT_SERVER_UPDATED: 'shortServerUpdated',
  SERVER_UPDATED: 'serverUpdated',
  WS_CONNECTED: "wsConnected",
}